/*global module:false*/
module.exports = function (grunt) {

  grunt.initConfig({
    pkg: grunt.file.readJSON('package.json'),

    jshint: {
      options: {
        jshintrc: '.jshintrc',
      },
      'default': {
        src: ['*.js']
      }
    },

    // read CSS lint rules
    // https://github.com/stubbornella/csslint/wiki/Rules
    csslint: {
      options: {
        csslintrc: '.csslintrc'
      },
      src: ['code-box.css']
    },

    concat: {
      css: {
        options: {
          separator: '\n',
          stripBanners: true,
          banner: '/*! <%= pkg.name %> - <%= pkg.version %> */\n\n'
        },
        src: [
          'magnific-popup/magnific-popup.css',
          'prism/prism.css',
          'bower_components/sticky-right-top-icon/sticky-right-top-icon.css',
          'code-box.css'
        ],
        dest: 'dist/code-box.css'
      },
      js: {
        options: {
          separator: ';\n',
          stripBanners: false,
          banner: '/*! <%= pkg.name %> - <%= pkg.version %> ' +
          'built on <%= grunt.template.today("yyyy-mm-dd") %>\n' +
          'author: <%= pkg.author %>, support: @bahmutov */\n\n'
        },
        src: [
          'magnific-popup/jquery.magnific-popup.js',
          'prism/prism.js',
          'bower_components/sticky-right-top-icon/sticky-right-top-icon.js',
          'code-box.js'
        ],
        dest: 'dist/code-box.js'
      }
    },

    replace: {
      dist: {
        options: {
          variables: {
            version: 'version: <%= pkg.version %>',
            timestamp: 'timestamp: <%= grunt.template.today() %>'
          },
          prefix: '@@'
        },
        files: {
          'dist/index.html': 'index.html'
        }
      }
    },

    copy: {
      main: {
        files: {
          'dist/README.md': 'README.md',
          'dist/CHANGES.md': 'CHANGES.md',
          'dist/bower.json': 'bower.json',
          'dist/normalize.css': 'bower_components/normalize-css/normalize.css'
        }
      }
    }
  });

  var plugins = require('matchdep').filterDev('grunt-*');
  plugins.forEach(grunt.loadNpmTasks);

  grunt.registerTask('default', ['jshint', 'csslint', 'concat:css', 'concat:js', 'replace', 'copy']);
};