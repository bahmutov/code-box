# code-box development

[![codeship status](https://www.codeship.io/projects/05459600-fed1-0130-26c1-6af3696257f1/status)](https://www.codeship.io/projects/05459600-fed1-0130-26c1-6af3696257f1/status)

## Environment

Requires nodejs, grunt and bower.

    npm install

## Building

    grunt

Should create folder dist with everything, including the example page *index.html*

The development repo is on [bitbucket](https://bitbucket.org/bahmutov/code-box), while the built version is hosted on [github](https://github.com/bahmutov/code-box) and distributed through bower as **code-box**.

Submit issues to [bitbucket repo](https://bitbucket.org/bahmutov/code-box), or directly to **@bahmutov**.

## Testing

End to end testing is done using [casperjs](http://casperjs.org/) on top of [phantomjs](http://phantomjs.org/). To verify installation run:

    phantomjs -v
    // 1.9.2
    casperjs --version
    // 1.1.0-DEV

Run end to end tests using command `casperjs test test/basic.js`

### Small print

Author: Gleb Bahmutov <gleb.bahmutov@gmail.com> [@bahmutov](https://twitter.com/bahmutov)
[![endorse](https://api.coderwall.com/bahmutov/endorsecount.png)](https://coderwall.com/bahmutov)

License: MIT, copyright &copy; 2013 Gleb Bahmutov
