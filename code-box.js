/*
  Adds buttons to code elements that open popup with same code,
  but with syntax highlighting.
*/
window.CodeBox = function (elementSelector) {
  if (!jQuery || !$) {
    throw new Error('CodeBox needs jQuery!');
  }

  function resetHeightIfOverflowing($el) {
    if (parseInt($el.css('marginTop'), 10) < -window.innerHeight * 0.4) {
      $el.css({
        'paddingTop': '0',
        'marginTop': '0',
        'top': '0'
      });
    }
  }

  function resetWidthIfOverflowing($el) {
    if (parseInt($el.css('marginLeft'), 10) < -window.innerWidth * 0.4) {
      $el.css({
        'paddingLeft': '20px',
        'paddingRight': '20px',
        'marginLeft': '0',
        'left': '0'
      });
    }
  }

  function toMiddle() {
    var $el = $(this);
    $el.css({
      'display' : 'inline',
      'position' : 'absolute',
      'left' : '50%',
      'top' : '50%',
      'paddingLeft': '0',
      'paddingTop': '0'
    }).css('marginLeft', function () {
      return - (+$el.width()) / 2;
    }).css('marginTop', function () {
      return - (+$el.height()) / 2;
    });

    resetHeightIfOverflowing($el);
    resetWidthIfOverflowing($el);
  }

  function centerHorizontally() {
    this.content.height(window.innerHeight * 0.88);

    var codeElements = $(this.content).children('.code-box-lightbox-middle');
    codeElements.each(toMiddle);
  }

  function getCode(element) {
    // usually code is inside <pre><code>...</code></pre>
    var code = element.innerText ||
      $(element).children('code')[0].innerHTML;
    return code;
  }

  function getLanguage($el) {
    var language = $el.children('code').attr('language') ||
      $el.children('code').attr('lang');
    if (!language) {
      language = $el.children('code').attr('class');
      if (language) {
        language = language.replace(/^lang-/, '');
      }
    }
    if (!language) {
      language = 'javascript';
    }
    return language;
  }

  // codeBlock should be formatted <pre>....</pre> string
  function makeCodeBoxPopup($button, codeBlock) {
    // see magnific-popup docs
    // http://dimsemenov.com/plugins/magnific-popup/documentation.html

    var codeInTheMiddle = '<div class="code-box-lightbox-middle">' + codeBlock.toString() + '</div>';
    var popupCode = '<div class="code-box-lightbox-code">' + codeInTheMiddle + '</div>';
    $button.magnificPopup({
      closeBtnInside: false,
      midClick: true,
      items: {
        src: popupCode,
        type: 'inline',
      },
      callbacks: {
        open: centerHorizontally
      }
    });
  }

  // adds small icon on top of $el to open code box
  /*
  function makeButton($el) {
    var zoomin = $('<span class="code-box-expand-icon"></span>');
    zoomin.attr('title', 'See code fullscreen');
    $el.append(zoomin);
    return  zoomin;
  }
  */

  function makeCodeBox() {
    var $el = $(this);
    /*global addTopRightIcon:true*/
    var zoomin = addTopRightIcon($el);
    zoomin.addClass('code-box-expand-icon');
    zoomin.attr('title', 'See code fullscreen');
    // var zoomin = makeButton($el);

    var code = getCode(this);
    var language = getLanguage($el);
    var highlighted = code;

    /*global Prism:true*/
    if (!Prism.languages[language]) {
      language = 'javascript';
    }
    if (Prism.languages[language]) {
      // http://prismjs.com/extending.html#api
      highlighted = Prism.highlight(code, Prism.languages[language]);
    }

    var codeBlock = '<pre>' + highlighted + '</pre>';
    makeCodeBoxPopup(zoomin, codeBlock);
  }

  function makeMultipleCodeBoxes(elementSelector) {
    if (!elementSelector) {
      throw new Error('missing code box element selector, for example "pre"');
    }

    $(elementSelector).each(makeCodeBox);
  }

  makeMultipleCodeBoxes(elementSelector);
};