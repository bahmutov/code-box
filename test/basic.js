// this test works with CasperjS 1.0.2
var casper = require('casper').create({
    verbose: true,
    logLevel: "debug"
});

casper.on('http.status.404', function (resource) {
  this.echo('Resource at ' + resource.url + ' not found (404)', 'ERROR');
});

casper.on('error', function (msg, backtrace) {
  this.echo('error ' + msg + '\n' + backtrace, 'ERROR');
});

casper.on('load.failed', function (obj) {
  this.echo('load failed ' + obj, 'ERROR');
});

casper.start('dist/index.html', function () {
  this.test.assertEval(function () {
    return __utils__.findAll('pre').length === 5;
  }, 'number of PRE elements');
});

casper.then(function () {
  this.test.assertEval(function () {
    return __utils__.findAll('.code-box-expand-icon').length === 5;
  }, 'number of code icons elements');
});

casper.run(function () {
  this.test.renderResults(true);
  casper.exit();
});
