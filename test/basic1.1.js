// this test works with CasperJs 1.1.0-DEV
var casper = require('casper').create({
    verbose: true,
    logLevel: "debug"
});

casper.test.begin('several code blocks', function (test) {
  casper.start('dist/index.html', function () {
    var n = 5;
    test.assertElementCount('pre', n);
    test.assertElementCount('.code-box-expand-icon', n);
  });

  casper.run(function () {
    test.done();
    casper.exit();
  });
});